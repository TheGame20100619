package com.xxl;

import android.app.Activity;
import android.os.Bundle;
import com.google.android.maps.MapView;
import com.google.android.maps.MapActivity;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Overlay;
import android.graphics.drawable.Drawable;

import java.util.List;

// public class LbgActivity extends Activity
public class LbgActivity extends MapActivity
{
    /** Called when the activity is first created. */
    private LbgItemizedOverlay lbgOverlayItems;

    public void onCreate(Bundle savedInstanceState)
    {
	MapView mapView;
	GeoPoint malmo;
	OverlayItem item;
	List<Overlay> mapOverlays;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

	mapView = (MapView) findViewById(R.id.mapview);
	mapView.setBuiltInZoomControls(true);

	mapOverlays = mapView.getOverlays();
	Drawable drawable = this.getResources().getDrawable(R.drawable.androidmarker);
	lbgOverlayItems = new LbgItemizedOverlay(drawable, this);

	malmo = new GeoPoint(55704471, 13201071);
	item = new OverlayItem(malmo, "Malmo", "malmo");
	lbgOverlayItems.addOverlay(item);
	mapOverlays.add(lbgOverlayItems);
    }

    protected boolean isRouteDisplayed()
    {
	return false;
    }
}
