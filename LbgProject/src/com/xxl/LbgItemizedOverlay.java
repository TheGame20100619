package com.xxl;

import java.util.ArrayList;
import android.content.Context;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

import android.app.AlertDialog;
import android.graphics.drawable.Drawable;

public class LbgItemizedOverlay extends ItemizedOverlay
{
    private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
    private Context mContext = null;

    public LbgItemizedOverlay(Drawable defaultMarker, Context context)
    {
	super(boundCenterBottom(defaultMarker));
	mContext = context;
    }

    public void addOverlay(OverlayItem overlay) {
	mOverlays.add(overlay);
	populate();
    }

    protected OverlayItem createItem(int i) {
	return mOverlays.get(i);
    }

    public int size() {
	return mOverlays.size();
    }

    protected boolean onTap(int index) {
	OverlayItem item = mOverlays.get(index);
	AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
	dialog.setTitle(item.getTitle());
	dialog.setMessage(item.getSnippet());
	dialog.show();
	return true;
    }
}